The aim of this project is to understand and implement Classification Techniques of Machine Learning to classify whether a patient has a 
heart disease or not according to the symptomatic features in the dataset with a class label making it a supervised, classification problem. 

Four classification models, Naïve Bayes, KNN, Logistic Regression and Random Forest have been implemented and it was found that Random Forest
performs with the best accuracy to predict the heart disease. Python has been used to create the solution.

The python solution is attached as CW_AML.py file. It is a class containing multiple functions with unique tasks. The program reads the attached
dataset Heart_Disease.csv first, then performs exploratory data analysis, data normalization, outliers detection and treatment, features' correlations,
and ultimately implementation of Machine Learning models with evaluation metrics.

The solution is documented in the attached report Heart_Disease_Supervised_Classification.pdf explaining the solution and displayig the results.
The code is attached as an Appendix. 

The code uses normal Python Libraries like seaborn, matplotlib, pandas, numpy and sklearn. To run this code, the system should have these packages
installed. Once done, run the python code either by command line or from any IDE like PyCharm placing the dataset in the same path as the code file.